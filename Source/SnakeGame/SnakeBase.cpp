// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h" 

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	DefaultLength = 4;
	MovementSpeed = 0.5;
	SpeedScale = 1;
	LastMoveDirection = EMovementDirection::DOWN;
	BlockMoveDirection = EMovementDirection::DOWN;
	MeshSize = ElementSize;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	//SetActorTickInterval(MovementSpeed);
	AddSnakeElement(DefaultLength);
	//Actor (Food) spawner
	const FVector ActorLocation(ElementSize * 3, ElementSize * 3, 0);
	const FRotator ActorRotation = GetActorRotation();
	GetWorld()->SpawnActor<AActor>(ActorToSpawn, ActorLocation, ActorRotation);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();	
	SetActorTickInterval(MovementSpeed);
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for(int i = 0; i<ElementsNum; i++)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->MeshComponent->SetVisibility(false);
		NewSnakeElem->SnakeOwner = this;
		if (MovementSpeed < 0.9)
		{
			MovementSpeed = MovementSpeed - ElementsNum * SpeedScale * 0.005;
		}
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}	
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		BlockMoveDirection = EMovementDirection::UP;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		BlockMoveDirection = EMovementDirection::DOWN;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		BlockMoveDirection = EMovementDirection::LEFT;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		BlockMoveDirection = EMovementDirection::RIGHT;
		break;
	}

	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	for (int i = 0; i < SnakeElements.Num(); i++)
	{
		SnakeElements[i]->MeshComponent->SetVisibility(true);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}